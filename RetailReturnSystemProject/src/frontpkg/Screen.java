package frontpkg;

//package frontpkg;
import java.awt.event.ActionListener;
import java.awt.*;
import java.awt.event.*;

import java.awt.Container;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JButton;

//import frontpkg.XXFrontEnd;

/**    This class demonstrates the Main Screen for the System
 *
 */
//public class Screen extends Jrame implements ActionListener{
public class Screen implements ActionListener{

	// Initialize all swing objects.
	//private JFrame f = new JFrame("Retail Checkout Return System"); //create Frame
	JFrame f = new JFrame("RETAIL CHECKOUT RETURN SYSTEM"); //create Frame

	private JButton btnSNT = new JButton("New Transaction");
	//btnSNT.setFont = new Font("Arial",Font.BOLD, 18);
	// btnSNT.setBackground(Color. red);

	private JButton btnRT = new JButton("Return Transaction");
	private JButton btnAddP = new JButton("Add Product");
	private JButton btnAddPS = new JButton("Add Pricing Scheme");
	private JButton btnCat = new JButton("Add Category");


	// Menu
	private MenuBar mb = new MenuBar(); // Menubar
	private Menu mnuFile = new Menu("File"); // File Entry on Menu bar
	private MenuItem mnuItemQuit = new MenuItem("Quit"); // Quit sub item
	private Menu mnuHelp = new Menu("Help"); // Help Menu entry
	private MenuItem mnuItemAbout = new MenuItem("About"); // About Entry

	/** Constructor for the GUI */
	public Screen(){
		// Set menubar
		f.setMenuBar(mb);

		//Build Menus
		mnuFile.add(mnuItemQuit);  // Create Quit line
		mnuHelp.add(mnuItemAbout); // Create About line
		mb.add(mnuFile);        // Add Menu items to form
		mb.add(mnuHelp);

		Container container = (Container) f.getContentPane();
		container.setLayout(null);

		container.add(btnSNT);
		btnSNT.setBounds(150, 20, 200, 50);
		btnSNT.addActionListener(this);

		container.add(btnAddP);
		btnAddP.setBounds(150, 100, 200, 50);
		btnAddP.addActionListener(this);

		container.add(btnAddPS);
		btnAddPS.setBounds(150, 180, 200, 50);
		btnAddPS.addActionListener(this);

		container.add(btnCat);
		btnCat.setBounds(150, 260, 200, 50);
		btnCat.addActionListener(this);

		container.add(btnRT);
		btnRT.setBounds(150, 340, 200, 50);
		btnRT.addActionListener(this);


		// Allows the Swing App to be closed
		f.addWindowListener(new ListenCloseWdw());

		//Add Menu listener
		mnuItemQuit.addActionListener(new ListenMenuQuit());

	}

	public class ListenMenuQuit implements ActionListener{
		public void actionPerformed(ActionEvent e){
			System.exit(0);         
		}
	}
	public class ListenCloseWdw extends WindowAdapter{
		public void windowClosing(WindowEvent e){
			System.exit(0);         
		}
	}

	public void launchFrame(){
		// Display Frame
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.pack(); //Adjusts panel to components for display
		f.setVisible(true);

		f.setBackground(Color.BLACK);

		f.setResizable(false);

	}

	/*public static void main(String args[]){
      Screen gui = new Screen();
      gui.launchFrame();

 }*/

	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource()==btnSNT){

			try {
				XXFrontEnd frame = new XXFrontEnd();
				frame.myFrame.setBounds(500, 500, 500, 500);
				frame.myFrame.setLocation(450,180);
				frame.myFrame.setVisible(true);
			} catch (Exception ex) {
				ex.printStackTrace();
			}		
		}
		else if(e.getSource()==btnAddP){
			AddProductScreen frame = new AddProductScreen();
			frame.f.setLocation(450,180);
			frame.f.setSize(500, 400);
			frame.f.setVisible(true);

		}
		else if(e.getSource()==btnCat){
			AddCategoryScreen frame = new AddCategoryScreen();
			frame.f.setLocation(450,180);
			frame.f.setSize(500, 400);
			frame.f.setVisible(true);

		}
		else if(e.getSource()==btnAddPS){
			AddPricingScheme frame = new AddPricingScheme();
			frame.f.setLocation(450,180);
			frame.f.setSize(500, 400);
			frame.f.setVisible(true);

		}
	}
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() 
			{
				try 
				{
					Screen frame = new Screen();
					frame.f.setLocation(450,180);
					frame.f.setSize(500, 500);
					frame.f.setVisible(true);
				} 
				catch (Exception e) 
				{

				}
			}
		});
	}
}