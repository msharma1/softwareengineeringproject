package frontpkg;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.SortedSet;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import backpkg.CategorySet;
import backpkg.PriceScheme;
import backpkg.Product;
import javax.swing.SwingConstants;

public class ReceiptScreen extends JFrame {
	private JTable receiptTable;
	private JTextField textSubtotal;
	private JTextField textTax;
	private JTextField textTotal;
	private JFrame receiptFrame;
	private DefaultTableModel tblModelReceipt;
	private JLabel lblSubTotal;
	private JLabel lblTax;
	private JLabel lblTotal;
	private SortedSet<CategorySet> ssp;
	protected double taxSubtotal=0;
	protected double subTotal;
	protected double receiptTotal=0;
	protected int totalQuantity;
	private int receiptID;
	private JTextField textReceiptID;
	
	public ReceiptScreen(int receiptID,SortedSet<CategorySet> ssp){
		this.receiptID=receiptID;
		this.ssp = ssp;
		receiptFrame = new JFrame();
		receiptFrame.setDefaultCloseOperation(receiptFrame.DISPOSE_ON_CLOSE);
		receiptFrame.setSize(500, 500);
		receiptFrame.setResizable(false);
		receiptFrame.setVisible(true);

		Container receiptContainer = (Container) receiptFrame.getContentPane();
		receiptContainer.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();	
		//		contentPane.add(scrollPane, BorderLayout.SOUTH);
		scrollPane.setBounds(10, 70,464, 201);
		receiptContainer.add(scrollPane);

		receiptTable = new JTable();
		
		scrollPane.setViewportView(receiptTable);
		tblModelReceipt = new DefaultTableModel();
		tblModelReceipt.addColumn("Category");
		tblModelReceipt.addColumn("Quantity");
		tblModelReceipt.addColumn("Product");
		tblModelReceipt.addColumn("Sales Tax");
		tblModelReceipt.addColumn("Price");
		receiptTable.setModel(tblModelReceipt);	




		textSubtotal = new JTextField();
		textSubtotal.setBounds(389,282, 85, 21);
		receiptContainer.add(textSubtotal);

		textTax = new JTextField();
		textTax.setBounds(389,314, 85, 21);
		receiptContainer.add(textTax);


		textTotal = new JTextField();
		textTotal.setBounds(388, 346, 86, 20);
		receiptContainer.add(textTotal);
		textTotal.setColumns(10);

		lblTotal = new JLabel("Total");
		lblTotal.setBounds(346, 349, 32, 14);
		receiptContainer.add(lblTotal);
		
		lblSubTotal = new JLabel("Subtotal");
		lblSubTotal.setBounds(332, 285, 47, 14);
		receiptContainer.add(lblSubTotal);
		
		lblTax = new JLabel("Tax");
		lblTax.setBounds(347, 317, 32, 14);
		receiptContainer.add(lblTax);
		
		textReceiptID = new JTextField();
		textReceiptID.setText(Integer.toString(receiptID));
		textReceiptID.setHorizontalAlignment(SwingConstants.RIGHT);
		textReceiptID.setBounds(67, 21, 93, 21);
		receiptContainer.add(textReceiptID);
		
		JLabel lblReceiptId = new JLabel("Receipt ID");
		lblReceiptId.setBounds(10, 24, 57, 14);
		receiptContainer.add(lblReceiptId);
		printReceipt();

	}
	
	
	@SuppressWarnings("static-access")
	public void printReceipt(){
		Iterator<CategorySet> cats = ssp.iterator();
		while(cats.hasNext()){

			CategorySet c = cats.next();
			int cat = -1;
			for(Product p : c.getCategoryProducts()){
				Double price = p.getQuanPrice().getPrice();
				Double tax = p.getQuanPrice().getTax();
				if(p.getCategoryID() != cat){
					Object[] rowdata =  {p.getCategoryDesc()};
				tblModelReceipt.addRow(rowdata);
				}

				Object[] rowdata =  {"",p.getQuanPrice().getQuantity(),p.getProductDescription(),tax,price};
				tblModelReceipt.addRow(rowdata);
				//this may or may not be expanded to list out the individual product for 
				//each of the quantity. This would also mean the price for each individual 
				//quantity needs to be listed.
				System.out.println();
				subTotal += price;
				cat = p.getCategoryID();
			}
		}
		textSubtotal.setText(Double.toString(subTotal));
		textSubtotal.setHorizontalAlignment(textSubtotal.RIGHT);
		taxSubtotal = subTotal*.05;
		textTax.setText(Double.toString(taxSubtotal));
		textTax.setHorizontalAlignment(textTax.RIGHT);
		Double total = subTotal + taxSubtotal;
		textTotal.setText(Double.toString(total));
		textTotal.setHorizontalAlignment(textTotal.RIGHT);

	}
}
