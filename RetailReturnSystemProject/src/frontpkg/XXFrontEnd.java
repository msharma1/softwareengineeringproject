package frontpkg;

import java.awt.Container;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.table.DefaultTableModel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JButton;
import backpkg.RetailReturnSystem;
import backpkg.Product;
import backpkg.PriceScheme;
import backpkg.QuanPrice;
import javax.swing.JLabel;

@SuppressWarnings("serial")
public class XXFrontEnd extends JFrame implements ActionListener {

	//private JPanel contentPane;
	private JTable table;
	private JTextField textProduct;
	private JTextField quanProduct;
	private JButton btnAdd;
	private JButton checkOut;
	JFrame myFrame;
	DefaultTableModel tblModel;
	private RetailReturnSystem rrs = new RetailReturnSystem();
	private JTextField textTotal;
	double totalprice = 0;

	JFrame frame = new JFrame();

	double subtotal = 0.0;
	double taxSubtotal = 0.0;


	/**
	 * Launch the application.
	 */
	//......................
/*	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					XXFrontEnd frame = new XXFrontEnd();
					frame.myFrame.setBounds(500, 500, 500, 500);
					frame.myFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*///.....................

	/**
	 * Create the frame.
	 */
	@SuppressWarnings("static-access")
	public XXFrontEnd() {

		myFrame = new JFrame();
		myFrame.setSize(500, 500);
		myFrame.setResizable(false);


		Container container = (Container) myFrame.getContentPane();
		container.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		//		contentPane.add(scrollPane, BorderLayout.SOUTH);
		scrollPane.setBounds(10, 70,464, 201);
		container.add(scrollPane);

		table = new JTable();
		scrollPane.setViewportView(table);
		tblModel = new DefaultTableModel();
		tblModel.addColumn("Product Name");
		tblModel.addColumn("Quantity");
		tblModel.addColumn("Unit Price");
		tblModel.addColumn("Sales Tax");
		tblModel.addColumn("Final Price");
		table.setModel(tblModel);	




		textProduct = new JTextField();
		textProduct.setBounds(57,15, 85, 21);
		//		contentPane.add(textField);
		container.add(textProduct);
		//	textField.setColumns(1);

		quanProduct = new JTextField();
		quanProduct.setBounds(57,40, 85, 21);
		//		contentPane.add(textField);
		container.add(quanProduct);


		btnAdd = new JButton("Scan Product");
		//		contentPane.add(btnNewButton, BorderLayout.EAST);
		container.add(btnAdd);
		btnAdd.setBounds(152, 11, 120, 25);
		btnAdd.addActionListener(this);
		//		textProduct.addActionListener(this);

		checkOut = new JButton("Checkout");
		//		contentPane.add(btnNewButton, BorderLayout.EAST);
		container.add(checkOut);
		checkOut.setBounds(300, 11, 120, 25);
		checkOut.addActionListener(this);

		textTotal = new JTextField();
		textTotal.setBounds(388, 282, 86, 20);
		container.add(textTotal);
		textTotal.setColumns(10);

		JLabel lblTotal = new JLabel("Total");
		lblTotal.setBounds(346, 285, 32, 14);
		container.add(lblTotal);


	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		// TODO Auto-generated method stub
		try
		{
			try{
				if(e.getSource()==btnAdd){
					//populate table
					//					System.out.println(btnAdd.getText());
					try{
						int productID = Integer.parseInt(textProduct.getText());
						int quantity = 1;


						String quanText = quanProduct.getText();
						if (!quanText.equals("")){
							quantity = Integer.parseInt(quanText);
						}
				


						Product p = rrs.loadProduct(productID, true);
						PriceScheme[]schemes = rrs.loadPriceScheme(p.getPricingSchemeID(), true);
						for(int i = 1; i<=quantity; i++){
							rrs.loadQuanPrice(p, 1);

							tblModel.addRow(tabledata(p, schemes,1));
							Object[] rowData = tabledata(p, schemes,1); 
							subtotal = subtotal + (Double) rowData[2];
							taxSubtotal = taxSubtotal + (Double) rowData[3];
							totalprice = totalprice + (Double) rowData[4];
						}
						textProduct.setText("");
						quanProduct.setText("");
						textTotal.setText(Double.toString(totalprice));
						textProduct.requestFocusInWindow();
					}
					catch(NumberFormatException nfe){
						new InputErrorWindow("Please enter a valid integer in the text field(s).");
						nfe.printStackTrace();
					}
				}
				else if(e.getSource()==checkOut){
					int quantity = tblModel.getRowCount();
					rrs.checkOut(subtotal, taxSubtotal,quantity);
					int tblSize = tblModel.getRowCount();
					for(int i = 0; i<tblSize; i++){
						tblModel.removeRow(0);
					}
					textTotal.setText("");
					totalprice = 0;
					subtotal = 0;
					taxSubtotal = 0;
				}

			}
			catch(Exception ex){
				ex.printStackTrace();
			}
		}
		catch(Exception excp)
		{
			excp.printStackTrace();
		}
	}

	private Object[] tabledata(Product p, PriceScheme[] ps,int quantity)
	{
		int total = p.getQuanPrice().getQuantity()-quantity;
		Object[] rowData = new Object[5];
		rowData[0] = p.getProductDescription();
		rowData[1] = quantity;
		for(int i = total; i<quantity+total;i++){
			Double price = (Double)rowData[2];
			if (price == null){
				price = 0.0;
			}
			rowData[2] = price + ps[i%ps.length].getPrice();

		}
		if(p.isIsTaxable()){
			Double tax = (Double) rowData[2];
			rowData[3] = tax*.05;
		}
		else{
			rowData[3] = 0.0;
		}
		rowData[4] = (Double)rowData[2] + (Double)rowData[3];
		return rowData;

		//change code in this format
		/*		private Object[] getLiquorRowData(Liquor liquor)
		{
		  Object[] rowData = new Object[4];
		  rowData[0] = new Integer(liquor.getId());
		  rowData[1] = liquor.getDescription();
		  rowData[2] = new Double(liquor.getPrice());
		  rowData[3] = new Integer(liquor.getAmount());
		  return rowData;
		} */
	}
}
