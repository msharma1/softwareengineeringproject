package frontpkg;

//import frontpkg;
import java.awt.event.ActionListener;
import java.awt.*;
import java.awt.event.*;

import java.awt.Container;
import java.awt.event.ActionEvent;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import backpkg.RetailReturnSystem;


/**    This class demonstrates the Main Screen for the System
*
*/
//public class Screen extends Jrame implements ActionListener{
public class AddProductScreen implements ActionListener{

  // Initialize all swing objects.
 JFrame f = new JFrame("ADD NEW PRODUCT"); //create Frame
 //private JTextField ProductID;
 private JTextField ProductDesc;
 private JComboBox PriceSchemeID;
 private JComboBox CategoryID;
 JComboBox Taxable;
// private JLabel labelProductID;
 private JLabel labelProductDesc;
 private JLabel labelPricingScheme;
 private JLabel labelCategory;
 private JLabel labelTaxable;
 private RetailReturnSystem rss = new RetailReturnSystem();
 
  private JButton button = new JButton("Add New Product");
 //btnSNT.setFont = new Font("Arial",Font.BOLD, 18);
 // btnSNT.setBackground(Color. red);

  // Menu
  private MenuBar mb = new MenuBar(); // Menubar
  private Menu mnuFile = new Menu("File"); // File Entry on Menu bar
  private MenuItem mnuItemQuit = new MenuItem("Quit"); // Quit sub item
  private Menu mnuHelp = new Menu("Help"); // Help Menu entry
  private MenuItem mnuItemAbout = new MenuItem("About"); // About Entry

  /** Constructor for the GUI */
  public AddProductScreen(){
     // Set menubar
      f.setMenuBar(mb);
     
      //Build Menus
      mnuFile.add(mnuItemQuit);  // Create Quit line
      mnuHelp.add(mnuItemAbout); // Create About line
      mb.add(mnuFile);        // Add Menu items to form
      mb.add(mnuHelp);
      
      Container container = (Container) f.getContentPane();
		container.setLayout(null);
		
		container.add(button);
		button.setBounds(150, 250, 200, 50);
		button.addActionListener(this);
		
//		ProductID = new JTextField();
//		ProductID.setBounds(280,50, 85, 30);
//		container.add(ProductID);
		
		ProductDesc = new JTextField();
		ProductDesc.setBounds(280,50, 180, 30);
		container.add(ProductDesc);
		
		
//		JLabel labelProductID = new JLabel("Product ID : ");
//		labelProductID.setBounds(60, 50, 85, 30);
//		container.add(labelProductID);
		
		PriceSchemeID = new JComboBox();
		for(Integer i : rss.getPriceSchemes()){
			PriceSchemeID.addItem(i);
		}
		PriceSchemeID.setBounds(280, 100, 100, 30);
		container.add(PriceSchemeID);
		
		
		CategoryID = new JComboBox();
		for(Integer i : rss.getCategory()){
			CategoryID.addItem(i);
		}
		CategoryID.setBounds(280, 150, 100, 30);
		container.add(CategoryID);
		
		Taxable = new JComboBox();
		Taxable.addItem("Yes");
		Taxable.addItem("No");
		Taxable.setBounds(280, 200, 100, 30);
		container.add(Taxable);
		
		labelProductDesc = new JLabel("Product Description : ");
		labelProductDesc.setBounds(60, 50, 200, 30);
		container.add(labelProductDesc);
		
		labelPricingScheme = new JLabel("Pricing Scheme ID : ");
		labelPricingScheme.setBounds(60, 100, 200, 30);
		container.add(labelPricingScheme);
		
		labelCategory = new JLabel("Category ID : ");
		labelCategory.setBounds(60, 150, 200, 30);
		container.add(labelCategory);
		
		labelTaxable = new JLabel("Apply Tax : ");
		labelTaxable.setBounds(60, 200, 200, 30);
		container.add(labelTaxable);
		
      // Allows the Swing App to be closed
      f.addWindowListener(new ListenCloseWdw());
      
      //Add Menu listener
      mnuItemQuit.addActionListener(new ListenMenuQuit());
      
  }
  
  public class ListenMenuQuit implements ActionListener{
  public void actionPerformed(ActionEvent e){
          System.exit(0);         
      }
  }
      public class ListenCloseWdw extends WindowAdapter{
      public void windowClosing(WindowEvent e){
          //System.exit(0);         
      }
  }
  
  public void launchFrame(){
      // Display Frame
//      f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      f.pack(); //Adjusts panel to components for display
      f.setVisible(true);
      
      f.setBackground(Color.BLACK);
      
		f.setResizable(false);
      
  }
  
  /*public static void main(String args[]){
      Screen gui = new Screen();
      gui.launchFrame();
     
 }*/
  
  public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource()==button){
					
					try{
					String isTaxable = (String)Taxable.getSelectedItem();
					Integer catID = (Integer) CategoryID.getSelectedItem();
					Integer psID = (Integer) PriceSchemeID.getSelectedItem();
					String desc = ProductDesc.getText();
					if (desc.length() == 0){
						throw new Exception();
					}
					if ("Yes".equals(isTaxable)){
						rss.createProduct(catID, psID, desc, true, true);
						f.dispose();
					}
					else
						rss.createProduct(catID, psID, desc, true, true);
						f.dispose();
					}
					catch(Exception ex){
						new InputErrorWindow("Please make sure all fields are filled.");
					}
		}
  }
  
}