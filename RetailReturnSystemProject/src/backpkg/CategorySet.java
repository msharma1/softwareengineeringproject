package backpkg;

import java.util.ArrayList;
/**
 * 
 */

/**
 * @author localuser
 * Each category has a set of Product objects stored as they are scanned.
 */
public class CategorySet implements Comparable {
	private String categoryName;
	private int categoryCount;
	private ArrayList<Product> products = new ArrayList<Product>();

	public CategorySet(Product product){
		products.add(product);
		setCategoryID(product.getCategoryID());
	}

	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public int getCategoryCount() {
		return categoryCount;
	}
	public void setCategoryID(int categoryCount) {
		this.categoryCount = categoryCount;
	}
	public void addProduct(Product product){
		products.add(product);
	}
	public ArrayList<Product> getCategoryProducts(){
		return products;
	}
	@Override
	public int compareTo(Object arg0) {
		CategorySet cs = (CategorySet)arg0;
		if(this.getCategoryCount()<cs.getCategoryCount()){
			return -1;
		}
		else if(this.getCategoryCount() == cs.getCategoryCount()){
			for(Product p : this.getCategoryProducts()){
				cs.addProduct(p);
			}
			return 0;
		}
		else{
			return 1;
		}

	}






}
