package backpkg;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.SortedSet;
import java.util.TreeSet;

import frontpkg.ReceiptScreen;


public class RetailReturnSystem {

	DBInteraction dbInter = new DBInteraction();
	private HashMap<Integer,Product> productQueue = new HashMap<Integer,Product>();
	private HashMap<Integer,PriceScheme[]> priceSchemes = new HashMap<Integer,PriceScheme[]>();

	/**
	 * Adds a product into the products table.
	 * Required fields are: CategoryID, PriceSchemeID, ProductName(description),
	 * and whether or not the product is taxable.
	 * @return uniquely generated ProductID from the database.
	 * @throws SQLException 
	 */
	public Integer createProduct(int categoryID, int pricingSchemeID, String productDescription, boolean isTaxable, boolean flag) throws SQLException 
	{
		Connection conn = dbInter.getConnection();
		int lastInsertedProductID = -1;

		try {
			CallableStatement cs = conn.prepareCall("{CALL CreateProduct(?, ?, ?, ?, ?)}");

			cs.setInt(1, categoryID);
			cs.setInt(2, pricingSchemeID);
			cs.setString(3, productDescription);
			cs.setBoolean(4, isTaxable);
			cs.registerOutParameter(5, java.sql.Types.INTEGER);
			cs.execute();
			lastInsertedProductID = cs.getInt(5);
			//System.out.println("Product created with productID: " + lastInsertedProductID);
		} 


		catch (SQLException e) {
			if (flag == true)
			{
				System.out.println("Error occurred while creating product. " + e.getMessage());
				System.out.println("Product: " + productDescription + " already exists.");
			}

			return null;
		}
		catch(NullPointerException e)
		{
			if (flag == true)
			{
				System.out.println(e.getMessage());
				System.out.println("Product: " + productDescription + " already exists.");
			}

			return null;
		}
		return lastInsertedProductID;

	}

	/**
	 * Returns the product with Name and Category name
	 * @param ProductID
	 * The product object contains Product description and Category description.
	 */
	
	public Integer createCategory(String categoryDescription, boolean flag) throws SQLException 
	{
		Connection conn = dbInter.getConnection();
		int lastInsertedCategoryID = -1;

		try {
			CallableStatement cs = conn.prepareCall("{CALL CreateCategory(?, ?)}");

			cs.setString(1, categoryDescription);
			cs.registerOutParameter(2, java.sql.Types.INTEGER);
			cs.execute();
			lastInsertedCategoryID = cs.getInt(2);
			//System.out.println("Category created with categoryID: " + lastInsertedCategoryID);
		} 


		catch (SQLException e) {
			if (flag == true)
			{
				System.out.println("Error occurred while creating category. " + e.getMessage());
				System.out.println("Category: " + categoryDescription + " already exists.");
			}

			return null;
		}
		catch(NullPointerException e)
		{
			if (flag == true)
			{
				System.out.println(e.getMessage());
				System.out.println("Category: " + categoryDescription + " already exists.");
			}

			return null;
		}
		return lastInsertedCategoryID;

	}

	
	public Product loadProduct(int ProductID, boolean flag)
	{	if(productQueue.containsKey(ProductID)){
		return productQueue.get(ProductID);
	}
	else{
		Connection conn = ((DBInteraction) dbInter).getConnection();
		Product product = new Product();
		product.setProductID(ProductID);
		try {
			CallableStatement cStmt = conn.prepareCall("{CALL LoadProduct(?)}");
			cStmt.setInt(1, ProductID);
			cStmt.execute();
			ResultSet rs1 = cStmt.getResultSet();
			while (rs1.next()) {

				product.setCategoryDesc(rs1.getString("CategoryDesc"));
				product.setProductDescription(rs1.getString("ProductDesc"));
				product.setCategoryID(rs1.getInt("CategoryID"));
				product.setPricingSchemeID(rs1.getInt("pricingSchemeID"));
				product.setIsTaxable(rs1.getInt("isTaxable") == 1);

			}
			rs1.close();
			if(product.getCategoryDesc() == null){
				throw new NullPointerException();
			}
			productQueue.put(product.getProductID(),product);

		} catch (SQLException e) {

			e.printStackTrace();
			return null;
		}
		catch (NullPointerException e) {

			System.out.println("Product with: " + product.getProductID() + " not found in the system");
			return null;
		}
		if (flag == true)
		{
			//			System.out.println("Product ID: " + product.getProductID() + " Product Name: "
			//					+ product.getProductDescription() + " CategoryID: " + product.getCategoryID()
			//					+ " Category: " + product.getCategoryDesc() + " PricingSchemeID: "
			//					+ product.getPricingSchemeID());
		}

		return product;
	}          
	}
	/**
	 * Removed a product by ProductID
	 * @param ProductID
	 * @return true or false based on whether the product was inserted
	 * @throws SQLException 
	 */
	public boolean removeProduct(int productID, boolean flag) throws SQLException
	{
		Connection conn = dbInter.getConnection();
		int row = -1;

		CallableStatement cStmt = conn.prepareCall("{CALL LoadProduct(?)}");
		cStmt.setInt(1, productID);
		cStmt.execute();
		ResultSet rs1 = cStmt.getResultSet();
		while (rs1.next()) {
			row = rs1.getInt(1);
		}
		if (row != -1)
		{
			try
			{
				CallableStatement cs = conn.prepareCall("{CALL removeProduct(?)}");
				cs.setInt(1, productID);
				cs.executeQuery();
			}
			catch(NullPointerException e)
			{
				System.out.println(e.getMessage());
				return false;
			}
			if (flag == true)
			{
				System.out.println("Product ID: " + productID + " removed successfully.");	
			}
			return true;
		}
		System.out.println("Product ID: " + productID + " was not found and could not be removed.");
		return false;

	}
	/**
	 * Updates Product
	 * @param ProductID, CategoryID, PricingSchemeID, ProductDesc, IsTaxable, flag
	 * @param flag determines whether or not to print the debugging message
	 * @return returns true or false based on whether or not the product was updated.
	 * @throws SQLException 
	 */
	public boolean storeProduct(int ProductID, int CategoryID, int PricingSchemeID, String ProductDesc, boolean IsTaxable, boolean flag) throws SQLException
	{
		Connection conn = dbInter.getConnection();
		int row = -1;
		CallableStatement cStmt = conn.prepareCall("{CALL LoadProduct(?)}");
		cStmt.setInt(1, ProductID);
		cStmt.execute();
		ResultSet rs1 = cStmt.getResultSet();
		while (rs1.next()) {
			row = rs1.getInt(1);
		}
		if (row != -1)
		{
			try {
				CallableStatement cs = conn.prepareCall("{CALL storeProduct(?, ?, ?, ?, ?)}");			
				cs.setInt(1, ProductID);
				cs.setInt(2, CategoryID);
				cs.setInt(3, PricingSchemeID);
				cs.setString(4, ProductDesc);
				cs.setBoolean(5, IsTaxable);
				cs.executeQuery();
				if (flag == true)
				{
					System.out.println("Product ID: " + ProductID + " updated to" +
							" CategoryID: " + CategoryID + " PricingSchemeID: " 
							+ PricingSchemeID + " Product Desc: " + ProductDesc 
							+ " IsTaxable: " + IsTaxable);

				}


			} 
			catch(NullPointerException e)
			{
				System.out.println(e.getMessage());
				return false;
			}

			catch (SQLException e) {
				System.out.println("Error occurred while updating product: " + e.getMessage());
				return false;
			}
			return true;
		}
		else
		{
			System.out.println("Supplied productID: " + ProductID + " not found in the database.");
			return false;	
		}

	}
	/**
	 * Adds a new pricing scheme
	 * @param Quantity, Price, and PricingSchemeDesc
	 * @return PricingSchemeID created in the database
	 * @throws SQLException 
	 */
	public Integer createPriceScheme(ArrayList<Double> quanPrice, String PricingSchemeDesc, boolean flag)
	{
		boolean isInsertable = true;
		Integer checkCount = quanPrice.size();

		Connection conn = dbInter.getConnection();
		Integer lastInsertedPriceSchemeID = null;

		//check if the pricingScheme being inserted is valid.


//		if (checkCount > 1)
//		{
//			//checks to see previous is always less than next for the quantity
//			for (int i = 0; i < checkCount-1; i ++ )
//			{
//				if(quanPrice.get(i).getQuantity() < quanPrice.get(i+1).getQuantity())
//				{
//					isInsertable = true;
//				}
//				else 
//				{
//					isInsertable = false;
//				}
//			}
//
//		}
//		//checks the special condition
//		//whether or not a single row can exists
//		else if (checkCount == 1) {
//			if(quanPrice.get(0).getQuantity() == 1)
//			{
//				isInsertable = true;
//			}
//			else {
//				isInsertable = false;
//			}
//		}


		//based on the previous condition checks, insert only if the conditions are met
		if (isInsertable == true)
		{
			try {
				CallableStatement cs = conn.prepareCall("{CALL CreatePriceScheme(?, ?)}");
				cs.setString(1, PricingSchemeDesc);
				cs.registerOutParameter(2, java.sql.Types.INTEGER);
				cs.executeQuery();	
				lastInsertedPriceSchemeID = cs.getInt(2);

				//inserts for every quantity
				for (int  j = 1 ; j <=checkCount; j++)
				{
					CallableStatement cs1 = conn.prepareCall("{CALL CreatePriceSchemePrices(?, ?, ?)}");
					cs1.setInt(1, lastInsertedPriceSchemeID);
					cs1.setInt(2, j);
					cs1.setDouble(3, quanPrice.get(j-1));
					cs1.executeQuery();

				}


			} 
			catch(NullPointerException e)
			{
				if (flag == true)
				{
					System.out.println(e.getMessage());
				}

				return null;
			}

			catch (SQLException e) {
				if (flag == true)
				{
					System.out.println("Error occurred while creating pricing Scheme. " + e.getMessage());
				}

				return null;
			}
			if (flag == true)
			{
				System.out.println("PricingSchemeID: " + lastInsertedPriceSchemeID + " was successfully created.");
			}
			return lastInsertedPriceSchemeID;
		}

		return null;
	}
	/**
	 * Removed a PricingScheme
	 * @param PricingSchemeID, flag
	 * @param flag - use with debugging only
	 * @return true or false
	 * @throws SQLException 
	 */
	public boolean removePriceScheme(int PriceSchemeID, boolean flag) throws SQLException
	{
		Connection conn = dbInter.getConnection();
		int row = -1;

		CallableStatement cStmt = conn.prepareCall("{CALL LoadPriceScheme(?)}");
		cStmt.setInt(1, PriceSchemeID);
		cStmt.execute();
		ResultSet rs1 = cStmt.getResultSet();
		while (rs1.next()) {
			row = rs1.getInt(1);
		}
		if (row != -1)
		{

			try {
				CallableStatement cs = conn.prepareCall("{CALL removePriceScheme(?)}");			
				cs.setInt(1, PriceSchemeID);
				cs.executeQuery();
				if (flag == true)
				{
					System.out.println("Price Scheme: " + PriceSchemeID + " successfully removed.");	
				}


			} 
			catch(NullPointerException e)
			{
				if (flag == true)
				{
					System.out.println(e.getMessage());
				}

				return false;
			}

			catch (SQLException e) {
				if (flag == true)
				{
					System.out.println("Error occurred while removing price Scheme: " + e.getMessage());
				}

				return false;
			}
			return true;

		}
		System.out.println("Price Scheme not found in the database.");
		return false;

	}
	/**
	 * Update PricingScheme
	 * @param priceSchemeID, Quantity, Price, PricingSchemeDesc, flag
	 * @param flag determines whether or not to print the debugging message
	 * @return true or false
	 * @throws SQLException 
	 */
	public boolean storePriceScheme(int priceSchemeID, int Quantity, float Price, String PricingSchemeDesc, boolean flag) throws SQLException
	{
		Connection conn = dbInter.getConnection();
		int row = -1;

		CallableStatement cStmt = conn.prepareCall("{CALL LoadPriceScheme(?)}");
		cStmt.setInt(1, priceSchemeID);
		cStmt.execute();
		ResultSet rs1 = cStmt.getResultSet();
		while (rs1.next()) {
			row = rs1.getInt(1);
		}
		if (row != -1)
		{
			try {
				CallableStatement cs = conn.prepareCall("{CALL storePriceScheme(?, ?, ?, ?)}");			
				cs.setInt(1, priceSchemeID);
				cs.setInt(2, Quantity);
				cs.setDouble(3, Price);
				cs.setString(4, PricingSchemeDesc);
				cs.executeQuery();
				if (flag == true)
				{
					System.out.println("Pricing Scheme for PriceSchemeID: " + priceSchemeID 
							+ " for quantity " + Quantity  + " updated to  Price: " 
							+ Price + " Description: " + PricingSchemeDesc);
				}

				return true;

			} 
			catch(NullPointerException e)
			{
				System.out.println(e.getMessage());
				return false;
			}

			catch (SQLException e) {
				System.out.println("Error occurred while updating Pricing Scheme: " + e.getMessage());
				return false;
			}	

		}
		System.out.println("Pricing Scheme ID: " + priceSchemeID + " was not found on the database.");
		return false;
	}

	/**
	 * Returns the pricing scheme info by PriceSchemeID
	 * @param productID
	 */
	public PriceScheme[] loadPriceScheme(int priceSchemeID, boolean flag)
	{	
		if(priceSchemes.containsKey(priceSchemeID)){
			return priceSchemes.get(priceSchemeID);
		}
		else{
			ArrayList<PriceScheme>pSchemes = new ArrayList<PriceScheme>();
			PriceScheme[]schemes;
			Connection conn = ((DBInteraction) dbInter).getConnection();
			PriceScheme priceScheme = new PriceScheme();
			int row = -1;

			try {
				CallableStatement cStmt = conn.prepareCall("{CALL LoadPriceScheme(?)}");
				cStmt.setInt(1, priceSchemeID);
				cStmt.execute();
				ResultSet rs1 = cStmt.getResultSet();
				while (rs1.next()) {
					row = rs1.getInt(1);
					if (row != -1)
					{
						priceScheme.setPricingSchemeID(rs1.getInt(1));
						priceScheme.setQuantity(rs1.getInt(2));
						priceScheme.setPrice(rs1.getDouble(3));
						priceScheme.setPriceSchemeDesc(rs1.getString(4));
						if (flag == true)
						{
							//							System.out.println("PricingSchemeID: " + rs1.getInt(1) + " " + ", Quantity: " + rs1.getInt(2)
							//									+ ", Price: " + rs1.getDouble(3) + ", Price Scheme description: " + rs1.getString(4));
						}
						pSchemes.add(priceScheme);

						priceScheme = new PriceScheme();
					}


				}
				rs1.close();

			} catch (SQLException e) {

				e.printStackTrace();
				return null;
			}
			catch(NullPointerException e)
			{
				System.out.println(e.getMessage());
				return null;
			}
			schemes = new PriceScheme[pSchemes.size()];
			for(int i = 0; i<schemes.length;i++){
				schemes[i]=pSchemes.get(i);
			}
			priceSchemes.put(priceSchemeID,schemes);
			return schemes;
		}
	}	

	/**
	 * Loads the proper quantity and price into the QuanPrice object of the given product
	 * @param p
	 * @param quantity
	 */

	public void loadQuanPrice(Product p, int quantity){
		PriceScheme[] ps = priceSchemes.get(p.getPricingSchemeID());
		int total = p.getQuanPrice().getQuantity();
		p.getQuanPrice().addItems(quantity);
		for(int i = total; i<quantity + total; i++){
			p.getQuanPrice().setPrice(ps[i%ps.length].getPrice(),p.isIsTaxable());

		}
	}

	public void checkOut(double subtotal, double tax, int quantity){
		SortedSet<CategorySet> finalList = new TreeSet<CategorySet>();
		Date date = new Date(System.currentTimeMillis());
		Timestamp timestamp = new Timestamp(date.getTime());
		
		try{
		int receiptID = createPurchaseReceipt(timestamp, tax, subtotal, subtotal+tax, quantity,true);
		
		for(Product p : productQueue.values()){
			storeProductRow(p,receiptID);
			finalList.add(new CategorySet(p));
		}
		new PurchaseReceipt(finalList);
		new ReceiptScreen(receiptID, finalList);
		productQueue = new HashMap<Integer, Product>();
		}
		catch(SQLException e){
			System.out.println(e.getMessage());
		}
	}

	public HashMap<Integer,Product> getProductQueue(){
		return productQueue;
	}

	/**
	 * Adds a receipt into the purchaseReceipt table.
	 * Required fields are: dateTime, taxSubtotal, subtotal, total, quantProds.
	 * @return uniquely generated purchaseReceiptID from the database.
	 * @throws SQLException 
	 */
	public Integer createPurchaseReceipt(Timestamp dateAndTime, double taxSubtotal, double subtotal, double receiptTotal, int totalQuantity, boolean flag) throws SQLException 
	{
		Connection conn = dbInter.getConnection();
		int lastInsertedReceiptID = -1;

		try {
			CallableStatement call = conn.prepareCall("{CALL CreateReceipt(?, ?, ?, ?, ?,?)}");

			
			
			call.setTimestamp(1,dateAndTime);
			call.setDouble(2, taxSubtotal);
			call.setDouble(3, subtotal);
			call.setDouble(4, receiptTotal);
			call.setDouble(5, totalQuantity);
			call.registerOutParameter(6, java.sql.Types.INTEGER);
			call.execute();
			lastInsertedReceiptID = call.getInt(6);

			System.out.println("Receipt created with productID: " + lastInsertedReceiptID); //Need to make sure access to auto-increment PK works
		} 


		catch (SQLException e) {
			if (flag == true)
			{
				System.out.println("Error occurred while creating receipt. " + e.getMessage());
			}

			return null;
		}
		catch(NullPointerException e)
		{
			if (flag == true)
			{
				System.out.println(e.getMessage());
			}

			return null;
		}
		return lastInsertedReceiptID;

	}

	/**
	 * Adds a purchased product to the purchaseProduct table.
	 * Required fields are: purchaseReceiptID, productID, price, prodCount, productDesc, isTaxable
	 * @param prodCount this is a sequential integer starting at 1 for each unique purchaseReceiptID.
	 * @return uniquely generated purchaseReceiptID from the database.
	 * @throws SQLException 
	 */

	public void createPurchasedProduct(int purchaseReceiptID, int productID, double price, int prodCount, String productDesc, double tax, boolean flag) throws SQLException
	{
		Connection conn = dbInter.getConnection();

		try {
			CallableStatement cpp = conn.prepareCall("{CALL CreatePurchasedProduct(?, ?, ?, ?, ?, ?)}");

			cpp.setInt(1, purchaseReceiptID);
			cpp.setInt(2, productID);
			cpp.setDouble(3, price);
			cpp.setInt(4, prodCount);
			cpp.setString(5, productDesc);
			cpp.setDouble(6, tax);

			cpp.execute();


		}catch (SQLException e) {
			if (flag == true)
			{
				System.out.println("Error occurred while creating purchased product. " + e.getMessage());
			}
			return;
		}
		catch(NullPointerException e)
		{
			if (flag == true)
			{
				System.out.println(e.getMessage());
			}
			return;
		}
		return;

	}

	public void storeProductRow(Product p, int receiptID){
		QuanPrice qp = p.getQuanPrice();
		int i = qp.getQuantity();
		for(int j = 1; j<=i; j++){
			double price = qp.getUnitPrice(j);
			double tax = 0.0;
			if(p.isIsTaxable()){
				tax = price * .05;
			}
			try{
			createPurchasedProduct(receiptID,p.getProductID(), price, j, p.getProductDescription(), tax, true);
			}
			catch(SQLException e){
				System.out.println(e.getStackTrace());
			}
		}
	}
	
	public ArrayList<Integer> getPriceSchemes(){
		Connection conn = ((DBInteraction) dbInter).getConnection();
		ArrayList<Integer> ps = new ArrayList<Integer>();
		try{
		PreparedStatement cStmt = conn.prepareStatement("select pricingschemeID from softwareengineering.pricingscheme");
		ResultSet rs = cStmt.executeQuery();
		while(rs.next()){
		ps.add(rs.getInt(1));
		}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return ps;
	}
	
	public ArrayList<Integer> getCategory(){
		Connection conn = ((DBInteraction) dbInter).getConnection();
		ArrayList<Integer> cat = new ArrayList<Integer>();
		try{
		PreparedStatement cStmt = conn.prepareStatement("select categoryID from softwareengineering.category");
		ResultSet rs = cStmt.executeQuery();
		while(rs.next()){
		cat.add(rs.getInt(1));
		}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return cat;
	}
}

