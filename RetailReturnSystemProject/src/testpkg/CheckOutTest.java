package testpkg;

import static org.junit.Assert.*;

import org.junit.Test;

import backpkg.PriceScheme;
import backpkg.Product;
import backpkg.RetailReturnSystem;


public class CheckOutTest {

	RetailReturnSystem rrs = new RetailReturnSystem();

	@Test
	public void testCheckOut(){

		Product p = rrs.loadProduct(1, true);
		rrs.loadPriceScheme(p.getPricingSchemeID(), true);
		for(int i = 1; i<=3; i++){
			rrs.loadQuanPrice(p, 1);
		}
		Product q = rrs.loadProduct(1, true);
		rrs.loadPriceScheme(q.getPricingSchemeID(), true);
		for(int i = 1; i<=3; i++){
			rrs.loadQuanPrice(q, 1);
		}

		//loaded several products into the 'Shopping Cart'

		rrs.checkOut();
		
		//test that after Check Out the products have been removed from cart.
		assertTrue(rrs.getProductQueue().get(1)==null);
		assertTrue(rrs.getProductQueue().get(2)==null);

	}


}


