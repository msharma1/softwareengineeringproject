package testpkg;


import static org.junit.Assert.*;
import backpkg.PriceScheme;
import backpkg.RetailReturnSystem;
import org.junit.*;


public class LoadPriceSchemeTest {

	
RetailReturnSystem rrs = new RetailReturnSystem();

	
@Test
public void testLoadPriceScheme1(){
PriceScheme [] ps = rrs.loadPriceScheme(90001, true);

assertTrue(ps[0].getPrice()==40.0);
assertTrue(ps[1].getPrice()==0.0);
}

@Test
public void testLoadPriceScheme2(){
PriceScheme [] ps = rrs.loadPriceScheme(90002, true);

assertTrue(ps[0].getPrice()==100.0);
assertTrue(ps[1].getPrice()==100.0);
assertTrue(ps[2].getPrice()==0.0);
}

@Test
public void testLoadPriceScheme3(){
PriceScheme [] ps = rrs.loadPriceScheme(90003, true);

assertTrue(ps[0].getPrice()==50.0);
assertTrue(ps[1].getPrice()==25.0);
}

@Test
public void testLoadPriceScheme4(){
PriceScheme [] ps = rrs.loadPriceScheme(90004, true);

assertTrue(ps[0].getPrice()==100.0);
assertTrue(ps[1].getPrice()==100.0);
assertTrue(ps[2].getPrice()==50.0);
}

}
